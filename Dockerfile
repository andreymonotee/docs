FROM squidfunk/mkdocs-material:6.1.6 as build
WORKDIR /build
COPY ./ru /build/ru
COPY ./en /build/en
COPY ./media /build/media
COPY ./stylesheets /build/stylesheets
RUN set -xe  \
    && cd /build/ru \
    && mkdocs build \
    && cd /build/en \
    && mkdocs build

FROM nginx:1.19-alpine
COPY ./nginx_default.conf /etc/nginx/conf.d/default.conf
COPY --from=build /build/ru/site /var/www/html/ru
COPY --from=build /build/en/site /var/www/html/en
LABEL io.yucca.docs=true

EXPOSE 8080
