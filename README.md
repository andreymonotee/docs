# Docs

User manual site

<https://docs.yucca.app>
<https://docs.yuccastream.com>

Для просмотра изменений:

```bash
make serve-ru
make serve-en
```

Build:

```bash
make
```

Push:

```bash
make
```
