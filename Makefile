ifeq ($(CI_REGISTRY_IMAGE),)
CI_REGISTRY_IMAGE = registry.gitlab.com/yuccastream/sites/docs
endif

ifeq ($(VERSION),)
VERSION = latest
endif

ifeq ($(CI_COMMIT_SHORT_SHA),)
CI_COMMIT_SHORT_SHA = $(shell git rev-parse --short HEAD)
endif

.DEFAULT_GOAL := build

.PHONY: build
build:
	@echo '\e[0;32m'"==> Build docker image"'\e[0m'
	docker build --force-rm \
		-t $(CI_REGISTRY_IMAGE):$(VERSION) \
		-t $(CI_REGISTRY_IMAGE):$(CI_COMMIT_SHORT_SHA) \
		-f Dockerfile .

.PHONY: push
push:
	docker push $(CI_REGISTRY_IMAGE):$(VERSION)
	docker push $(CI_REGISTRY_IMAGE):$(CI_COMMIT_SHORT_SHA)

.PHONY: serve-ru
serve-ru:
	docker run --rm -it -v $(PWD):/docs -w /docs -p 8000:8000 squidfunk/mkdocs-material:6.1.6 serve -f ./ru/mkdocs.yml -a 0.0.0.0:8000

.PHONY: serve-en
serve-en:
	docker run --rm -it -v $(PWD):/docs -w /docs -p 8000:8000 squidfunk/mkdocs-material:6.1.6 serve -f ./en/mkdocs.yml -a 0.0.0.0:8000
