# Too many open files

При большом количестве камер (>70), вы можете столкнуться с ограничениями [Inotify](https://ru.wikipedia.org/wiki/Inotify), тогда при добавлении камеры в логе Docker-контейнера можно увидеть сообщение:

`Error: too many open files`

По умолчанию в Ubuntu выставлены следующие параметры:

```bash
$ sysctl fs.inotify
fs.inotify.max_queued_events = 16384
fs.inotify.max_user_instances = 128
fs.inotify.max_user_watches = 524288
```

Чтобы увеличить лимиты, добавьте в файл `/etc/sysctl.conf` следующие параметры:

```bash
$ sudo su
$ cat << EOF >> /etc/sysctl.conf
fs.inotify.max_queued_events = 16384
fs.inotify.max_user_instances = 10240
fs.inotify.max_user_watches = 524288
EOF
```

Примените настройки:

```bash
sudo sysctl -p
```

Убедитесь, что настройки применены:

```bash
$ sysctl fs.inotify
fs.inotify.max_queued_events = 16384
fs.inotify.max_user_instances = 10240
fs.inotify.max_user_watches = 524288
```
