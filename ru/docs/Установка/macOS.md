---
title: Установка на macOS
---

# Установка на macOS

!!! notice "Работа Юкка на новых ARM-процессорах Apple M1"
    10 ноября 2020 года компания Apple [анонсировала](https://www.apple.com/mac/m1/) выход устройств на базе нового процессора, а для обеспечения обратной совместимости был представлен эмулятор [Rosetta 2](https://developer.apple.com/documentation/apple_silicon/about_the_rosetta_translation_environment).

    Мы не проводили тестирование Юкка на новых процессорах и не гарантируем корректную работу через эмулятор.

## Установка с Homebrew

Откройте терминал, выполните команды:

```shell
brew tap yuccastream/tap
brew install yucca
```

Во время инсталляции дополнительно будут установлены FFmpeg и Docker.

## Установка без Homebrew

Для работы потребуется Docker, инструкция для установки доступна на [официальном сайте](https://docs.docker.com/engine/install/).

Откройте терминал, скачайте нужные файлы и запустите сервер:

```shell
mkdir -p ./yucca
cd ./yucca
wget https://get.yucca.app/v0.4.0/darwin_amd64/yucca
wget https://get.yucca.app/v0.4.0/yucca.toml
chmod +x ./yucca
./yucca server --config ./yucca.toml
```

После запуска Web-интерфейс будет доступен по адресу http://ip-вашего-сервера:9910
