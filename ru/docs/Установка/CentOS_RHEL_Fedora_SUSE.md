---
title: Установка на CentOS/RHEL/Fedora/SUSE
---

# Установка на CentOS/RHEL/Fedora/SUSE

Создаём каталог для установки и переходим в него, скачиваем последнюю версию yucca, файл конфигурации и systemd unit-файл сервиса:

=== "amd64"

    ```bash
    sudo mkdir -p /opt/yucca/ffmpeg
    cd /opt/yucca
    sudo wget https://get.yucca.app/v0.4.0/linux_amd64/yucca -O /opt/yucca/yucca
    sudo chmod +x /opt/yucca/yucca
    sudo wget https://get.yucca.app/v0.4.0/yucca.toml -O /opt/yucca/yucca.toml
    sudo wget https://get.yucca.app/v0.4.0/yucca.service -O /opt/yucca/yucca.service
    ```

=== "arm"

    ```bash
    sudo mkdir -p /opt/yucca/ffmpeg
    cd /opt/yucca
    sudo wget https://get.yucca.app/v0.4.0/linux_arm/yucca -O /opt/yucca/yucca
    sudo chmod +x /opt/yucca/yucca
    sudo wget https://get.yucca.app/v0.4.0/yucca.toml -O /opt/yucca/yucca.toml
    sudo wget https://get.yucca.app/v0.4.0/yucca.service -O /opt/yucca/yucca.service
    ```

=== "arm64"

    ```bash
    sudo mkdir -p /opt/yucca/ffmpeg
    cd /opt/yucca
    sudo wget https://get.yucca.app/v0.4.0/linux_arm64/yucca -O /opt/yucca/yucca
    sudo chmod +x /opt/yucca/yucca
    sudo wget https://get.yucca.app/v0.4.0/yucca.toml -O /opt/yucca/yucca.toml
    sudo wget https://get.yucca.app/v0.4.0/yucca.service -O /opt/yucca/yucca.service
    ```

Устанаваем ffmpeg и ffprobe

!!! info "Для корректной работы необходима версия FFmpeg не ниже 4.0"
    Можно использовать статическую сборку предложенную нами, или любую другую при условии, что ffmpeg и ffprobe резовятся в окружении по стандартному имени

=== "amd64"

    ```bash
    cd /opt/yucca/ffmpeg
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-amd64-static.tar.xz
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-amd64-static.tar.xz.md5
    md5sum -c ffmpeg-4.3.1-amd64-static.tar.xz.md5
    sudo tar -xJvf ffmpeg-4.3.1-amd64-static.tar.xz ffmpeg-4.3.1-amd64-static/ffmpeg --strip-components 1
    sudo tar -xJvf ffmpeg-4.3.1-amd64-static.tar.xz ffmpeg-4.3.1-amd64-static/ffprobe --strip-components 1
    sudo rm -f ffmpeg-4.3.1-amd64-static.tar.xz*
    ```

=== "arm"

    ```bash
    cd /opt/yucca/ffmpeg
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-armhf-static.tar.xz
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-armhf-static.tar.xz.md5
    md5sum -c ffmpeg-4.3.1-armhf-static.tar.xz.md5
    sudo tar -xJvf ffmpeg-4.3.1-armhf-static.tar.xz ffmpeg-4.3.1-armhf-static/ffmpeg --strip-components 1
    sudo tar -xJvf ffmpeg-4.3.1-armhf-static.tar.xz ffmpeg-4.3.1-armhf-static/ffprobe --strip-components 1
    sudo rm -f ffmpeg-4.3.1-armhf-static.tar.xz*
    ```

=== "arm64"

    ```bash
    cd /opt/yucca/ffmpeg
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-arm64-static.tar.xz
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-arm64-static.tar.xz.md5
    md5sum -c ffmpeg-4.3.1-arm64-static.tar.xz.md5
    sudo tar -xJvf ffmpeg-4.3.1-arm64-static.tar.xz ffmpeg-4.3.1-arm64-static/ffmpeg --strip-components 1
    sudo tar -xJvf ffmpeg-4.3.1-arm64-static.tar.xz ffmpeg-4.3.1-arm64-static/ffprobe --strip-components 1
    sudo rm -f ffmpeg-4.3.1-arm64-static.tar.xz*
    ```

Настраиваем сетевой фильтр:

```bash
sudo firewall-cmd --permanent --new-service=yucca
sudo firewall-cmd --permanent --service=yucca --add-port=9910/tcp
sudo firewall-cmd --permanent --zone=public --add-service=yucca
sudo firewall-cmd --reload
```

После запуска Web-интерфейс будет доступен по адресу http://ip-вашего-сервера:9910
