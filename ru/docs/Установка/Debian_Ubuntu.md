---
title: Установка на Debian/Ubuntu
---

# Установка на Debian/Ubuntu

Мы рекомендуем устанавливать Юкка на операционную систему Ubuntu с долгосрочной поддержкой (LTS), например, 18.04 и старше. При этом работа возможна на любом другом дистрибутиве Linux. Так же для работы потребуется Docker, инструкция для установки доступна на [официальном сайте](https://docs.docker.com/engine/install/).

!!! info "Быстрая установка Docker"

    ```bash
    bash <(curl -sSL https://get.docker.com)
    ```

## Установка через пакетный менеджер

Скачайте и установите `.deb` пакет нужной версии и архитектуры, полный список доступных версий: <a href="https://get.yucca.app/" target="_blank">https://get.yucca.app/</a>

=== "amd64"

    ```bash
    wget https://get.yucca.app/v0.4.0/linux_amd64/yucca_0.4.0-1_amd64.deb
    sudo dpkg -i yucca_0.4.0-1_amd64.deb
    ```

=== "arm"

    ```bash
    wget https://get.yucca.app/v0.4.0/linux_arm/yucca_0.4.0-1_arm.deb
    sudo dpkg -i yucca_0.4.0-1_arm.deb
    ```

=== "arm64"

    ```bash
    wget https://get.yucca.app/v0.4.0/linux_arm64/yucca_0.4.0-1_arm64.deb
    sudo dpkg -i yucca_0.4.0-1_arm64.deb
    ```

После запуска WEB-интерфейс будет доступен по адресу http://ip-вашего-сервера:9910

## Установка вручную

Начнем с создания пользователя и группы, затем добавим нового пользователя в группу `docker`, чтобы иметь возможность работать с Docker-контейнерами:

```bash
sudo groupadd -f yucca
sudo useradd -M -s /bin/false -b /opt/yucca -g yucca -G docker yucca
```

Создаём каталог для установки и переходим в него, скачиваем последнюю версию yucca, файл конфигурации и systemd unit-файл сервиса:

=== "amd64"

    ```bash
    sudo mkdir -p /opt/yucca/ffmpeg
    cd /opt/yucca
    sudo wget https://get.yucca.app/v0.4.0/linux_amd64/yucca -O /opt/yucca/yucca
    sudo chmod +x /opt/yucca/yucca
    sudo wget https://get.yucca.app/v0.4.0/yucca.toml -O /opt/yucca/yucca.toml
    sudo wget https://get.yucca.app/v0.4.0/yucca.service -O /opt/yucca/yucca.service
    ```

=== "arm"

    ```bash
    sudo mkdir -p /opt/yucca/ffmpeg
    cd /opt/yucca
    sudo wget https://get.yucca.app/v0.4.0/linux_arm/yucca -O /opt/yucca/yucca
    sudo chmod +x /opt/yucca/yucca
    sudo wget https://get.yucca.app/v0.4.0/yucca.toml -O /opt/yucca/yucca.toml
    sudo wget https://get.yucca.app/v0.4.0/yucca.service -O /opt/yucca/yucca.service
    ```

=== "arm64"

    ```bash
    sudo mkdir -p /opt/yucca/ffmpeg
    cd /opt/yucca
    sudo wget https://get.yucca.app/v0.4.0/linux_arm64/yucca -O /opt/yucca/yucca
    sudo chmod +x /opt/yucca/yucca
    sudo wget https://get.yucca.app/v0.4.0/yucca.toml -O /opt/yucca/yucca.toml
    sudo wget https://get.yucca.app/v0.4.0/yucca.service -O /opt/yucca/yucca.service
    ```

Устанаваем ffmpeg и ffprobe

!!! info "Для корректной работы необходима версия FFmpeg не ниже 4.0"
    Можно использовать статическую сборку предложенную нами, или любую другую при условии, что ffmpeg и ffprobe резовятся в окружении по стандартному имени

=== "amd64"

    ```bash
    cd /opt/yucca/ffmpeg
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-amd64-static.tar.xz
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-amd64-static.tar.xz.md5
    md5sum -c ffmpeg-4.3.1-amd64-static.tar.xz.md5
    sudo tar -xJvf ffmpeg-4.3.1-amd64-static.tar.xz ffmpeg-4.3.1-amd64-static/ffmpeg --strip-components 1
    sudo tar -xJvf ffmpeg-4.3.1-amd64-static.tar.xz ffmpeg-4.3.1-amd64-static/ffprobe --strip-components 1
    sudo rm -f ffmpeg-4.3.1-amd64-static.tar.xz*
    ```

=== "arm"

    ```bash
    cd /opt/yucca/ffmpeg
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-armhf-static.tar.xz
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-armhf-static.tar.xz.md5
    md5sum -c ffmpeg-4.3.1-armhf-static.tar.xz.md5
    sudo tar -xJvf ffmpeg-4.3.1-armhf-static.tar.xz ffmpeg-4.3.1-armhf-static/ffmpeg --strip-components 1
    sudo tar -xJvf ffmpeg-4.3.1-armhf-static.tar.xz ffmpeg-4.3.1-armhf-static/ffprobe --strip-components 1
    sudo rm -f ffmpeg-4.3.1-armhf-static.tar.xz*
    ```

=== "arm64"

    ```bash
    cd /opt/yucca/ffmpeg
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-arm64-static.tar.xz
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-arm64-static.tar.xz.md5
    md5sum -c ffmpeg-4.3.1-arm64-static.tar.xz.md5
    sudo tar -xJvf ffmpeg-4.3.1-arm64-static.tar.xz ffmpeg-4.3.1-arm64-static/ffmpeg --strip-components 1
    sudo tar -xJvf ffmpeg-4.3.1-arm64-static.tar.xz ffmpeg-4.3.1-arm64-static/ffprobe --strip-components 1
    sudo rm -f ffmpeg-4.3.1-arm64-static.tar.xz*
    ```

Задаем владельца и права доступа:

```bash
sudo chown -R yucca:yucca /opt/yucca
sudo chmod -R 2775 /opt/yucca
```

Копируем systemd unit-файл в служебный каталог и обновляем список демонов, чтобы изменения вступили в силу:

```bash
sudo cp /opt/yucca/yucca.service /lib/systemd/system/
sudo systemctl daemon-reload
```

Теперь можно запустить сервер и проверить работу:

```bash
sudo systemctl enable yucca
sudo systemctl start yucca
```

После запуска Web-интерфейс будет доступен по адресу http://ip-вашего-сервера:9910
