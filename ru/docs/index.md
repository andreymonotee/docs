# Добро пожаловать

Вы находитесь на сайте с документацией видеорегистратора [Юкка](https://yucca.app) – программного обеспечения для организации видеонаблюдения.

## Быстрый старт

Установка и системные требования:

- [Debian или Ubuntu](/ru/Установка/Debian_Ubuntu/)
- [CentOS, Fedora, OpenSuse или Red Hat](/ru/Установка/CentOS_RHEL_Fedora_SUSE/)
- [macOS](/ru/Установка/macOS/)

## Нашли ошибку в тексте?

Если вы увидели ошибку или опечатку, помогите её исправить – напишите нам [на почту](mailto:info@yucca.app), в [Telegram-чат](https://t.me/yuccastream) или создайте Merge Resuest [в репозиторий](https://gitlab.com/yuccastream/sites/docs/).
