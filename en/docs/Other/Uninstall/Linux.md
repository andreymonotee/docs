# Linux

## Debian/Ubuntu

```bash
sudo apt purge yucca
```

When configuring by default, uninstalling the package does not delete all the archive datas, you have to remove the catalog.

```bash
sudo rm -rf /opt/yucca
```

## Uninstall manual

Stop and disable the server:

```bash
sudo systemctl stop yucca.service
sudo systemctl disable yucca.service
sudo rm -f /etc/systemd/system/yucca.service
sudo systemctl daemon-reload
```

Remove all the server files:

```bash
sudo rm -rf /opt/yucca
```
