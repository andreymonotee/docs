# Inotify

If you have a large number of streams (>70), you may encounter [Inotify](https://en.wikipedia.org/wiki/Inotify) restrictions. Then when creating new streams in the container log you can see message:

`Error: too many open files`

By default the following parameters are set in Ubuntu:

```bash
$ sysctl fs.inotify
fs.inotify.max_queued_events = 16384
fs.inotify.max_user_instances = 128
fs.inotify.max_user_watches = 524288
```

To increase the limits, add to file `/etc/sysctl.conf` the following parameters:

```bash
$ sudo su
$ cat << EOF >> /etc/sysctl.conf
fs.inotify.max_queued_events = 16384
fs.inotify.max_user_instances = 10240
fs.inotify.max_user_watches = 524288
EOF
```

and apply the settings:

```bash
sudo sysctl -p
```

check:

```bash
$ sysctl fs.inotify
fs.inotify.max_queued_events = 16384
fs.inotify.max_user_instances = 10240
fs.inotify.max_user_watches = 524288
```
