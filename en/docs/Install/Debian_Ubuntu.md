---
title: Installation on Debian/Ubuntu
---

# Installation on Debian/Ubuntu

We recommend installing Yucca on an Ubuntu Long Term Support (LTS) operating system such as 18.04 and older. In this case, work is possible on any other Linux distribution. You will also need Docker to work, installation instructions are available on the [official site](https://docs.docker.com/engine/install/).

!!! info "Quick install Docker"

    ```bash
    bash <(curl -sSL https://get.docker.com)
    ```

## Installation via the package manager

Download and install the `.deb` package of the required version and architecture, a complete list of available versions: <a href="https://get.yucca.app/" target="_blank"> https://get.yucca.app/</a>

=== "amd64"

    ```bash
    wget https://get.yucca.app/v0.4.0/linux_amd64/yucca_0.4.0-1_amd64.deb
    sudo dpkg -i yucca_0.4.0-1_amd64.deb
    ```

=== "arm"

    ```bash
    wget https://get.yucca.app/v0.4.0/linux_arm/yucca_0.4.0-1_arm.deb
    sudo dpkg -i yucca_0.4.0-1_arm.deb
    ```

=== "arm64"

    ```bash
    wget https://get.yucca.app/v0.4.0/linux_arm64/yucca_0.4.0-1_arm64.deb
    sudo dpkg -i yucca_0.4.0-1_arm64.deb
    ```

After starting the WEB-interface will be available at http://ip-your-server:9910

## Manual installation

Let's start by creating a user and a group, then add a new user to the `docker` group to be able to work with Docker containers:

```bash
sudo groupadd -f yucca
sudo useradd -M -s /bin/false -b /opt/yucca -g yucca -G docker yucca
```

Create a directory for installation and go to it, download the latest version of Yucca, a configuration file and a systemd service unit file:

=== "amd64"

    ```bash
    sudo mkdir -p /opt/yucca/ffmpeg
    cd /opt/yucca
    sudo wget https://get.yucca.app/v0.4.0/linux_amd64/yucca -O /opt/yucca/yucca
    sudo chmod +x /opt/yucca/yucca
    sudo wget https://get.yucca.app/v0.4.0/yucca.toml -O /opt/yucca/yucca.toml
    sudo wget https://get.yucca.app/v0.4.0/yucca.service -O /opt/yucca/yucca.service
    ```

=== "arm"

    ```bash
    sudo mkdir -p /opt/yucca/ffmpeg
    cd /opt/yucca
    sudo wget https://get.yucca.app/v0.4.0/linux_arm/yucca -O /opt/yucca/yucca
    sudo chmod +x /opt/yucca/yucca
    sudo wget https://get.yucca.app/v0.4.0/yucca.toml -O /opt/yucca/yucca.toml
    sudo wget https://get.yucca.app/v0.4.0/yucca.service -O /opt/yucca/yucca.service
    ```

=== "arm64"

    ```bash
    sudo mkdir -p /opt/yucca/ffmpeg
    cd /opt/yucca
    sudo wget https://get.yucca.app/v0.4.0/linux_arm64/yucca -O /opt/yucca/yucca
    sudo chmod +x /opt/yucca/yucca
    sudo wget https://get.yucca.app/v0.4.0/yucca.toml -O /opt/yucca/yucca.toml
    sudo wget https://get.yucca.app/v0.4.0/yucca.service -O /opt/yucca/yucca.service
    ```

Installation ffmpeg и ffprobe

!!! info "FFmpeg version not lower than 4.0"
    You can use the static assembly we suggested, or any other, provided that ffmpeg and ffprobe are resolved into env by a standard name

=== "amd64"

    ```bash
    cd /opt/yucca/ffmpeg
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-amd64-static.tar.xz
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-amd64-static.tar.xz.md5
    md5sum -c ffmpeg-4.3.1-amd64-static.tar.xz.md5
    sudo tar -xJvf ffmpeg-4.3.1-amd64-static.tar.xz ffmpeg-4.3.1-amd64-static/ffmpeg --strip-components 1
    sudo tar -xJvf ffmpeg-4.3.1-amd64-static.tar.xz ffmpeg-4.3.1-amd64-static/ffprobe --strip-components 1
    sudo rm -f ffmpeg-4.3.1-amd64-static.tar.xz*
    ```

=== "arm"

    ```bash
    cd /opt/yucca/ffmpeg
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-armhf-static.tar.xz
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-armhf-static.tar.xz.md5
    md5sum -c ffmpeg-4.3.1-armhf-static.tar.xz.md5
    sudo tar -xJvf ffmpeg-4.3.1-armhf-static.tar.xz ffmpeg-4.3.1-armhf-static/ffmpeg --strip-components 1
    sudo tar -xJvf ffmpeg-4.3.1-armhf-static.tar.xz ffmpeg-4.3.1-armhf-static/ffprobe --strip-components 1
    sudo rm -f ffmpeg-4.3.1-armhf-static.tar.xz*
    ```

=== "arm64"

    ```bash
    cd /opt/yucca/ffmpeg
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-arm64-static.tar.xz
    sudo wget https://get.yucca.app/ffmpeg/4.3.1/ffmpeg-4.3.1-arm64-static.tar.xz.md5
    md5sum -c ffmpeg-4.3.1-arm64-static.tar.xz.md5
    sudo tar -xJvf ffmpeg-4.3.1-arm64-static.tar.xz ffmpeg-4.3.1-arm64-static/ffmpeg --strip-components 1
    sudo tar -xJvf ffmpeg-4.3.1-arm64-static.tar.xz ffmpeg-4.3.1-arm64-static/ffprobe --strip-components 1
    sudo rm -f ffmpeg-4.3.1-arm64-static.tar.xz*
    ```

We set the owner and access rights:

```bash
sudo chown -R yucca:yucca /opt/yucca
sudo chmod -R 2775 /opt/yucca
```

Copy the systemd unit file to the service directory and update the list of daemons for the changes to take effect:

```bash
sudo cp /opt/yucca/yucca.service /lib/systemd/system/
sudo systemctl daemon-reload
```

Now you can start the server and test it:

```bash
sudo systemctl enable yucca
sudo systemctl start yucca
```

After launch, the Web interface will be available at http://ip-your-server:9910
