---
title: Installing on macOS
---

# Installation on macOS

!!! notice "Yucca works on new ARM-processors Apple M1"
    On November 10, 2020, Apple [announced](https://www.apple.com/mac/m1/) the release of devices based on the new processor and for backward compatibility emulator was introduced the [Rosetta 2](https://developer.apple.com/documentation/apple_silicon/about_the_rosetta_translation_environment).

    We have not tested Yucca on new processors and do not guarantee correct operation through the emulator.

## Installing with Homebrew

Open a terminal, run the commands:

```shell
brew tap yuccastream/tap
brew install yucca
```

During the installation, FFmpeg and Docker will be additionally installed.

## Install without Homebrew

Docker is required to work, installation instructions are available on the [official site](https://docs.docker.com/engine/install/).

Open a terminal, download the necessary files and start the server:

```shell
mkdir -p ./yucca
cd ./yucca
wget https://get.yucca.app/v0.4.0/darwin_amd64/yucca
wget https://get.yucca.app/v0.4.0/yucca.toml
chmod +x ./yucca
./yucca server --config ./yucca.toml
```

After launch the Web interface will be available at http://ip-your-server:9910