# Welcome

You are on the site with the documentation of the video recorder [Yucca](https://yucca.app) - software for organizing video surveillance.

## Quick start

Installation and system requirements:

- [Debian or Ubuntu](/en/Install/Debian_Ubuntu/)
- [CentOS, Fedora, OpenSuse or Red Hat](/en/Install/CentOS_RHEL_Fedora_SUSE/)
- [macOS](/en/Install/macOS/)

## Found a mistake in the text?

If you see an error or typo, help fix it - write to us [by mail](mailto:info@yucca.app), in [Telegram-chat](https://t.me/yuccastream) or create a Merge Resuest [in repository](https://gitlab.com/yuccastream/sites/docs/).
