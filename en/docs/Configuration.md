# Configuration

Yucca setting may be provided via configuration file, command line arguments or environment variables. Configuration read priority:

1. command line arguments;
2. file;
3. environment variables.

Settings is divided into logistic sections (blocks), which respond to prefix in command line arguments and environment variables.

In section Configuration file in square brackets `[]` indicate configuration block to which belongs the option.

## analytics

Analytics collection. Yucca server may collect and send marketing infomation. In particular, the information of system configuration (processor model, type and version of operation system, RAM size, disks size, number of launched streams) to which it launched. We DO NOT collect the information about the streams sources and other sensitive datas. Analytic collection helps us understand better our users.

Collection information is also available in the terms of use.

| <div style="width:230px">Command line arguments</div> | Configuration file | Environment variables  | Default | <div style="width:300px">Explanation</div> |
| :------------------------- | :---------------- | :------------------- | :-------------------- | :-------- |
| `--analytics` | [analytics] enabled | YUCCA_ANALYTICS_ENABLED | true | enable / disable |
| `--analytics-address` | [analytics] address | YUCCA_ANALYTICS_ADDRESS | "https://analytics.yucca.app/" | URL to which send information |

## branding

Branding allows you to change logo, field title and favicon.

!!! info "Available only in Enterprise"

| <div style="width:230px">Command line arguments</div>   | Cofiguration file  | Environment variables  | Default  | <div style="width:300px">Explanation</div>  |
| :------------------------- | :---------------- | :------------------- | :-------------------- | :-------- |
| `--branding-app-logo` | [branding] app_logo | YUCCA_BRANDING_APP_LOGO | "" | Path to file with logo  |
| `--branding-app-title` | [branding] app_title | YUCCA_BRANDING_APP_TITLE | "" | Text in the field Title  |
| `--branding-fav-icon` | [branding] fav_icon | YUCCA_BRANDING_FAV_ICON | "" | Path to file with favicon  |

## database

Connecting to database.

Supports 2 types of databases in which it stores the states:

- sqlite3
- postgres

In small installations it’s enough sqlite3, for a large number of streams we recommend to use postgres.

| <div style="width:230px">Command line arguments</div> | configuration file | Environment variables | Default | <div style="width:300px">Explanation</div> |
| :------------------------- | :---------------- | :------------------- | :-------------------- | :-------- |
| `--database-type` | [database] type | YUCCA_DATABASE_TYPE | "sqlite3" |  Type of using database (sqlite3 or postgres)  |
| `--database-path` | [database] path | YUCCA_DATABASE_PATH | "" |  Path to database file (only sqlite3)  |
| `--database-host` | [database] host | YUCCA_DATABASE_HOST | "127.0.0.1:5432" |  Connection address to database (only for postgres)  |
| `--database-name` | [database] name | YUCCA_DATABASE_NAME | "yucca" |  Database name (only for postgres)  |
| `--database-user` | [database] user | YUCCA_DATABASE_USER | "postgres" |  User name for connection (only for postgres)  |
| `--database-password` | [database] password | YUCCA_DATABASE_PASSWORD | "postgres" |  User password for connection (only for postgres)  |
| `--database-ssl-mode` | [database] ssl_mode | YUCCA_DATABASE_SSL_MODE | "disable" |  Use when connecting to SSL(uplink connection) (only for postgres) Possible values (disable, require, verify-full)  |
| `--database-busy-timeout` | [database] busy_timeout | YUCCA_DATABASE_BUSY_TIMEOUT | 500 |  Time-out in seconds for waiting unblocking the table SQLite (only for sqlite3)  |
| `--database-ca-cert-path` | [database] ca_cert_path | YUCCA_DATABASE_CA_CERT_PATH | "/etc/ssl/certs" |  Path to Certification  Center Certificate in case of using SSL (only for postgres)  |
| `--database-client-cert-path` | [database] client_cert_path | YUCCA_DATABASE_CLIENT_CERT_PATH | "" |  Path to file with certificate in case of using SSL (only for postgres)  |
| `--database-client-key-path` | [database] client_key_path | YUCCA_DATABASE_CLIENT_KEY_PATH | "" |  Path to file with key in case of using SSL (only for postgres)  |
| `--database-cache-mode` | [database] cache_mode | YUCCA_DATABASE_CACHE_MODE | "shared" |  Cache mode option using for database connection (only for sqlite3) Possible values (private, shared)   |
| `--database-conn-max-lifetime` | [database] conn_max_lifetime | YUCCA_DATABASE_CONN_MAX_LIFETIME | "0s" |  Sets the maximum time during which a connection can be repeated  |
| `--database-log-queries` | [database] log_queries | YUCCA_DATABASE_LOG_QUERIES | false |  Log sql queries and  runtime  |
| `--database-max-open-conn` | [database] max_open_conn | YUCCA_DATABASE_MAX_OPEN_CONN | 0 |  Maximum number of opened connections with database (only for postgres) |
| `--database-max-idle-conn` | [database] max_idle_conn | YUCCA_DATABASE_MAX_IDLE_CONN | 2 |  Maximum number of connections while awaiting (only for postgres)  |

## executor

Streams settings launch.

| <div style="width:230px">Command line arguments</div> | Configuration file| Environment variables| Default | <div style="width:300px">Explanation</div>  |
| :------------------------- | :---------------- | :------------------- | :-------------------- | :-------- |
| `--executor` | [executor] type | YUCCA_EXECUTOR_TYPE | "docker" |  Executor type for parking streams. Possible values (docker) |
| `--executor-log-level` | [executor] log_level | YUCCA_EXECUTOR_LOG_LEVEL | "info" |  Logging level. Possible values (panic, fatal, error, warning, info, debug)  |
| `--docker-image` | [executor.docker] image | YUCCA_EXECUTOR_DOCKER_IMAGE | "registry.gitlab.com/yuccastream/yucca" |  The name of the image docker for worl as an executor.  |
| `--docker-cap-add` | [executor.docker] cap_add | YUCCA_EXECUTOR_DOCKER_CAP_ADD | [] |  Put Linux capabilities for docker container.  |
| `--docker-cap-drop` | [executor.docker] cap_drop | YUCCA_EXECUTOR_DOCKER_CAP_DROP | [] |  Drop Linux  capabilities for docker cotainer.  |
| `--docker-cpu-shares` | [executor.docker] cpu_shares | YUCCA_EXECUTOR_DOCKER_CPU_SHARES | 0 |  CPU limit consumption for container. CPU quotas (relative weight compared to other containers) |
| `--docker-dns` | [executor.docker] dns | YUCCA_EXECUTOR_DOCKER_DNS | [] |  DNS-server list for using by container.  |
| `--docker-dns-search` | [executor.docker] dns_search | YUCCA_EXECUTOR_DOCKER_DNS_SEARCH | [] |  DNS-domains list for searching  |
| `--docker-memory` | [executor.docker] memory | YUCCA_EXECUTOR_DOCKER_MEMORY | 0 |  Memory limit for container.  |
| `--docker-memory-reservation` | [executor.docker] memory_reservation | YUCCA_EXECUTOR_DOCKER_MEMORY_RESERVATION | 0 |  Memory soft limit   |
| `--docker-memory-swap` | [executor.docker] memory_swap | YUCCA_EXECUTOR_DOCKER_MEMORY_SWAP | 0 |  Total memory limit(memory + swap).  |
| `--docker-nano-cpus` | [executor.docker] nano_cpus | YUCCA_EXECUTOR_DOCKER_NANO_CPUS | 0 |  CPU quota in units.  |
| `--docker-network-driver` | [executor.docker] network_driver | YUCCA_EXECUTOR_DOCKER_NETWORK_DRIVER | "bridge" |  Network driver Docker (https://docs.docker.com/network/) |
| `--docker-oom-kill-disable` | [executor.docker] oom_kill_disable | YUCCA_EXECUTOR_DOCKER_OOM_KILL_DISABLE | false |  Do not destroy processes in the container if there is a memory shortages (OOM) |
| `--docker-oom-score-adjust` | [executor.docker] oom_score_adjust | YUCCA_EXECUTOR_DOCKER_OOM_SCORE_ADJUST | 0 |  OOM Indicators setting  |
| `--docker-security-opt` | [executor.docker] security_opt | YUCCA_EXECUTOR_DOCKER_SECURITY_OPT | [] |  List of string values for setting  MLS system tags such as SELinux  |

## quotas

Global quotas on resourses. Can limit maximum number of certain resources that can be created\added as part of the installation.

!!! info "Available only in Enterprise"

| <div style="width:230px">Command line arguments</div> | Configuration file| Environment variables| Default | <div style="width:300px">Explanation</div>  |
| :------------------------- | :---------------- | :------------------- | :-------------------- | :-------- |
| `--quota-global-stream` | [quota] global_stream | YUCCA_QUOTA_GLOBAL_STREAM | -1 |  Quota on making streams (-1 without limits)  |
| `--quota-global-team` | [quota] global_team | YUCCA_QUOTA_GLOBAL_TEAM | -1 |  Quota on making commands (-1 without limits)  |
| `--quota-global-user` | [quota] global_user | YUCCA_QUOTA_GLOBAL_USER | -1 |  Quota on making users (-1 without limits)  |

## server

Server global settings.

| <div style="width:230px">Command line arguments</div> | Configuration file| Environment variables| Default | <div style="width:300px">Explanation</div>  |
| :------------------------- | :---------------- | :------------------- | :-------------------- | :-------- |
| `--log-json` | - | - | true | Print logs in JSON format  |
| `--log-level` | - | - | "info" | Logging level. Possible values (panic, fatal, error, warning, info, debug)  |
| `--help` | - | - | - | Run help  Yucca Server |
| `--config` | - | - | "/etc/yucca/yucca.toml" | Path to configuration file  |
| `--data-dir` | [server] data_dir | YUCCA_SERVER_DATA_DIR | "" | Global directory for storing state (in case of using sqlite3) and streams data. Makes by default with name `data` in the same path as a binary file Yucca. |
| `--alloc-dir` | [server] alloc_dir | YUCCA_SERVER_ALLOC_DIR | "" | Streams datas directory (video archive etc..) Makes by default in data_dir, named `alloc`. |
| `--state-dir` | [server] state_dir | YUCCA_SERVER_STATE_DIR | "" | Directory with database file (in case of using sqlite3) Makes by default in data_dir, named `server`  |
| `--default-language` | [server] default_language |YUCCA_SERVER_DEFAULT_LANGUAGE | "en" | Default interface language for new users  |
| `--web.listen-address` | [server] listen_address | YUCCA_SERVER_LISTEN_ADDRESS | "127.0.0.1:9910" | IP address and port where the webinterface will be available  |
| `--cert-file` | [server] cert_file | YUCCA_SERVER_CERT_FILE | "" | Path to certificate file in case of using HTTPS  |
| `--cert-key` | [server] cert_key | YUCCA_SERVER_CERT_KEY | "" | Path to key file in case of using HTTPS  |
| `--pprof` |[server] pprof | YUCCA_SERVER_PPROF | true | Debug mode |

## streams

Settings related to stream behavior.

| <div style="width:230px">Command line arguments</div> | Configuration file| Environment variables| Default | <div style="width:300px">Explanation</div>  |
| :------------------------- | :---------------- | :------------------- | :-------------------- | :-------- |
| `--streams-archive-download-max-duration` | [streams] archive_download_max_duration | YUCCA_STREAMS_ARCHIVE_DOWNLOAD_MAX_DURATION | "24h" | Maximum allowable length of requested download time |
| `--streams-playlist-max-duration` | [streams] playlist_max_duration | YUCCA_STREAMS_PLAYLIST_MAX_DURATION | "3h" | Maximum allowable length of requested time for m3u8 playlist  |
| `--streams-ranges-max-duration` | [streams] ranges_max_duration | YUCCA_STREAMS_RANGES_MAX_DURATION | "168h" | Maximum allowable length of time requested for ranges (archive range availability)  |
| `--streams-uuid-slug` | [streams] uuid_slug | YUCCA_STREAMS_UUID_SLUG | false | Use as a public stream identifier generated by UUID  |

## telemetry

Uses for monitoring settings.

| <div style="width:230px">Command line arguments</div> | Configuration file| Environment variables| Default | <div style="width:300px">Explanation</div>  |
| :------------------------- | :---------------- | :------------------- | :-------------------- | :-------- |
| `--telemetry` | [telemetry] enabled | YUCCA_TELEMETRY_ENABLED | true | enable / disable the telemetry  |
| `--web.telemetry-path` | [telemetry] path | YUCCA_TELEMETRY_PATH | "/metrics" | URL for telemetry access  |
| `--telemetry-basic-auth-username` | [telemetry] basic_auth_username | YUCCA_TELEMETRY_BASIC_AUTH_USERNAME | "" | Based authorisation login for telemetry access  |
| `--telemetry-basic-auth-password` | [telemetry] basic_auth_password | YUCCA_TELEMETRY_BASIC_AUTH_PASSWORD | "" | Based authorisation password for telemetry access  |
