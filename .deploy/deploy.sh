#!/usr/bin/env bash

# set -xe

SCRIPT_DIR="$(dirname $(readlink -f $0))"

CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-'registry.gitlab.com/yuccastream/sites/docs'}
CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA:-$(git rev-parse --short HEAD)}
VERSION=${VERSION:-'latest'}

help_info() {
  echo -e "Первый аргумент указывает, деплоить или удалять \n"
  echo -e "production-apply | production-remove"
  echo -e "staging-apply | staging-remove"
}

echo_url() {
  local URL="${1}"
  echo -e '\e[0;33m' "URL:" '\e[0;32m' "${URL}" '\e[0m'
}

deploy() {
    local ACTION="${1}"
    local ENV="${2}"
    if [[ "${CI}" != "true" ]]; then
      cd ${SCRIPT_DIR}/../
      make build
      make push
    fi
    cd ${SCRIPT_DIR}/k8s/overlays/${ENV}
    git checkout @ -- kustomization.yaml
    kustomize edit set image registry.gitlab.com/yuccastream/sites/docs=${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
    kustomize build
    kustomize build | kubectl ${ACTION} -f-
    git checkout @ -- kustomization.yaml
    case "${ENV}" in
      production ) echo_url "https://docs.yucca.app" ;;
      staging ) echo_url "https://staging-docs.yucca.app" ;;
    esac
}

case "${1}" in
    staging-apply ) deploy apply staging ;;
    staging-remove ) deploy delete staging ;;
    production-apply ) deploy apply production ;;
    production-remove ) deploy delete production ;;
    * ) help_info ;;
esac
